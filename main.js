let images = document.querySelectorAll('.image-to-show');
let stopBtn = document.querySelector('.stop-btn');
let restoreBtn = document.querySelector('.restore-btn');
let currentIndex = 0;
let timerInterval = 10;
let output = 300;
let timer = document.getElementById('timer');

function startTimer() {
  pushTimer = setInterval(() => {
    timer.textContent = `До показу наступного зображення залишилось ${output-- / 100}`;
    if (output === 0) {
      output = 300;
      timer.textContent = output-- / 100;
      showImages()
    }
  }, timerInterval);
}
startTimer()


function showImages() {
  images[currentIndex].style.display = 'none';
  currentIndex = (currentIndex + 1) % images.length; // якщо ми досягли кінця списку блоків, ми повертаємося до першого блоку за допомогою оператора %.
  images[currentIndex].style.display = 'block';
}

function pauseShowImages() {
  clearInterval(pushTimer)
}

stopBtn.addEventListener('click', pauseShowImages)
restoreBtn.addEventListener('click', startTimer)


let themeToggler = document.querySelector('#theme-toggler');

themeToggler.addEventListener('click', function () {
  document.body.classList.toggle('dark-mode');
  if (document.body.classList.contains('dark-mode')) {
    localStorage.setItem('website-theme', 'dark-mode');
  } else {
    localStorage.setItem('website-theme', 'default');
  }
});

function retrieveTheme() {
  let theme = localStorage.getItem('website-theme');
  if (theme != null) {
    document.body.classList.remove('default', 'dark-mode'); document.body.classList.add(theme);
  }
}

retrieveTheme();